﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



/* 
 play with the values for max speed and acceleration
 to get a feel for how they work. It would also be 
 helpful to adjust the linear drag (rigidbody2D.LinearDrag).
 this will determine how fast forces will decay when force 
 is not being applied in a direction. You should be able
 to do this through scripting as well. a line like
 rigidbody2DComponent.LinearDrag = x; would work.

    GOOD LUCK!!
     */
public class Movement : MonoBehaviour
{
    float horizontalAxis;
    float verticalAxis;

    public float maxSpeed = 5;        //When you want the cube to stop accelerating
    public float acceleration = 0.5f; //however fast you want the object to accelerate
    float verticalMovement;    //Will be updated every frame. how fast are we moving on x axis
    float horizontalMovement;  // same as abobe but y axis

    Vector2 moveSpeed;

    Rigidbody2D rigidbody2DComponent;

    // Start is called before the first frame update
    void Start()
    {
        rigidbody2DComponent = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        horizontalAxis = Input.GetAxis("Horizontal");     //This will set the float variable to 1 or -1 
        verticalAxis = Input.GetAxis("Vertical");         //depending on what input you are giving

        if (verticalAxis > 0) //Vertical movement to the right
            //> means bigger than. the open side of the sign is always the bigger number.
        {

            // variable += x; |is the same as| variable = variable + x;


            verticalMovement += verticalAxis * acceleration;  //vertical movement is equal to whatever it was plus (the input of the player times the acceleration rate)

            //Mathf.Abs() will return the absolute value of a variable. If x = -5 then Mathf.Abs(x); will return positive 5;
            //                                                          If x = 5  then Mathf.Abs(x); will still return positive 5;

            if (Mathf.Abs(verticalMovement) > maxSpeed)  //Mathf.Abs not needed on this one, but it looks more uniform
            {
                verticalMovement = maxSpeed; //if the current vertical speed is more than the max speed, make the current speed whatever max speed is.
            }
        }
        else if (verticalAxis < 0)//Vertical movement to the left
        {
            verticalMovement += verticalAxis * acceleration;

            if (Mathf.Abs(verticalMovement) > maxSpeed)
            {
                verticalMovement = -maxSpeed;
            }
        }
        else //if you have no vertical input, there should be no more vertical force added to the object. This will not stop the object in its tracks. It will have a decaying force from previous frames
        {
            verticalMovement = 0;

        }
        if (horizontalAxis > 0) //Horizontal movement -- same as above but in on the y axis
        {
            horizontalMovement += horizontalAxis * acceleration;

            if (Mathf.Abs(horizontalMovement) > maxSpeed)
            {
                horizontalMovement = maxSpeed;
            }
        }
        else if (horizontalAxis < 0)
        {
            horizontalMovement += horizontalAxis * acceleration;


            if (Mathf.Abs(horizontalMovement) > maxSpeed)
            {
                horizontalMovement = -maxSpeed;
            }
        }
        else
        {
            horizontalMovement = 0;

        }
        //the code up to this point tells how much you want to push 
        //the object up OR down and left OR right. If you put them
        //In a Vector2 it tells you what angle you want to push
        //the object is as well as the amount of force to be applied
        moveSpeed = new Vector2(horizontalMovement, verticalMovement); 
    }

    void FixedUpdate()
    {
        //This will apply a force to the object in the desired direction
        //of the player. Time.deltaTime will ensure that it moves at
        //a speed determined by time and not by how fast the computer
        //can process each frame.
        rigidbody2DComponent.AddForce(moveSpeed * Time.deltaTime);
    }
}
