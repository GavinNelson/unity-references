﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/*
 When making your animations, you first have to build the animations in unity. 
 The scripting part will only be used for logic.

    go to window > animation > animator and animation
    both are neeeded to make animations

    make the logic inside the animator

    use the same variables in from the animator in here, it will make things easier and clearer.

    GOODLUCK!
     */
public class Animations : MonoBehaviour
{
    bool up;
    bool down;
    bool left;
    bool right;

    Animator animatorComponent;

    // Start is called before the first frame update
    void Awake()
    {
        animatorComponent = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        //Basic logic for animations. play with this, try something more interesting perhaps
        if (Input.GetKeyDown("w"))
        {
            up = true;
        }
        else
        {
            up = false;
        }

        if (Input.GetKeyDown("s"))
        {
            down = true;
        }
        else
        {
            down = false;
        }
        if (Input.GetKeyDown("d"))
        {
            right = true;
        }
        else
        {
            right = false;
        }
        if (Input.GetKeyDown("a"))
        {
            left = true;
        }
        else
        {
            left = false;
        }

        //The animator component.set a boolean variable("name of the variable in the animator", new value of animator variable);

        animatorComponent.SetBool("up", up);
        animatorComponent.SetBool("down", down);
        animatorComponent.SetBool("left", left);
        animatorComponent.SetBool("right", right);

    }
}
